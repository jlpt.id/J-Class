import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyA70PzUhPWtRLBXfB2YXZS6LhS8xdZXeGg',
  authDomain: 'test-project-3d462.firebaseapp.com',
  databaseURL: 'https://test-project-3d462.firebaseio.com',
  projectId: 'test-project-3d462',
  storageBucket: 'test-project-3d462.appspot.com',
  messagingSenderId: '593607953340',
  appId: '1:593607953340:web:494c25241f6ee95637b9b9',
};

firebase.initializeApp(firebaseConfig);

// utils
const db = firebase.firestore();
const auth = firebase.auth();

// collection references
const usersCollection = db.collection('users');
const materiesCollection = db.collection('materies');
const questionCollection = db.collection('question');
const scoresCollection = db.collection('scores');

// export utils/refs
export {
  db,
  auth,
  usersCollection,
  materiesCollection,
  questionCollection,
  scoresCollection,
};
