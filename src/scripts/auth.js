/**
 * --------------------------------------
 * 
 * Authorization script
 * 
 * Semua script untuk hubungan database
 * ditulis disini
 * 
 * Database : Firebase
 * 
 * --------------------------------------
 */

import * as firebase from '@/firebase.js';

export default {
  name: 'authScript',
  LogIn(email, password, autoLogin) { 

    const login = firebase.auth.signInWithEmailAndPassword(email, password);

    // firebase login
    return login.then(function(user){
      localStorage.setItem("profile", user);
      localStorage.setItem("userEmail",email);
      
      // set auto login
      if(autoLogin === true){
        localStorage.setItem("userPass", password);
      }else{
        localStorage.setItem("userPass", "");
      }
      
      return Promise.resolve(user);
    })
    .catch(function(error) {
      // membuat log, jika ada masalah, log akan dikirim ke maintener jika dibutuhkan
      var d = new Date();
      var curr_date = d.getDate();
      var curr_month = d.getMonth();
      var curr_year = d.getFullYear();
      localStorage.setItem(
        "userLogError",
        localStorage.getItem("userLogError") + ',' +
        JSON.stringify({
          date: curr_date + "/" + curr_month + "/" + curr_year,
          type: "LOGIN",
          email: email,
          authError: error,
        })
      );

      if(error.code === "auth/user-not-found"){
        return Promise.reject("Akun belum terdaftar!");
      }else{
        return Promise.reject("Email atau Kata Sandi Salah");
      }

    });

  },
  EmailRegistration(email, password, name, reason, address) { 
    const signUp = firebase.auth.createUserWithEmailAndPassword(email, password);
    // promise start
    return signUp.then(function(user) {
      // membuat data sementara pada userinfo
      firebase.usersCollection.add({
        uid: user.user.uid,
        name: name,
        reason: '',
        address: '',
        accountLevel: 'free',
        privilege: 'users',
        picture: 0,
        phone: 0,
        verified: 0,
      })
  
      // return true ketika email berhasil didaftarkan
      return Promise.resolve(user);
    })
    .catch(function(error) {
      // membuat log, jika ada masalah, log akan dikirim ke maintener jika dibutuhkan
      var d = new Date();
      var curr_date = d.getDate();
      var curr_month = d.getMonth();
      var curr_year = d.getFullYear();
      localStorage.setItem(
        "userLogError",
        localStorage.getItem("userLogError") + ',' +
        JSON.stringify({
          date: curr_date + "/" + curr_month + "/" + curr_year,
          type: "REGISTRATION",
          email: email,
          authError: error,
        })
      );

      // return error ketika ada kesalahan pada saat pendaftaran
      if(error.code === 'auth/email-already-in-use'){
        return Promise.reject("Email kamu udah terdaftar");
      }else{
        return Promise.reject("Ada kesalahan!");
      }
    });
  },
};
