/* eslint no-param-reassign: ["error", { "props": false }] */
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
const store = new Vuex.Store({
  state: {
    posts: [],
  },
  mutations: {
    setPosts(state, val) {
      state.posts = val
    },
  },
  modules: {
    authentication: {
      namespaced: true,
      state: {
        userProfile: {},
        firstRegistration: 0,
      },
      mutations: {
        setUserProfile(state, val) {
          state.userProfile = val;
        },
      },
    },
    splitter: {
      namespaced: true,
      state: {
        open: false,
      },
      mutations: {
        toggle(state, shouldOpen) {
          if (typeof shouldOpen === 'boolean') {
            state.open = shouldOpen;
          } else {
            state.open = !state.open;
          }
        },
      },
    },
    navigator: {
      namespaced: true,
      state: {
        stack: [],
        open: false,
        postId: '',
        quizId: '',
      },
      mutations: {
        push(state, page) {
          state.stack.push(page);
        },
        pop(state) {
          if (state.stack.length > 1) {
            state.stack.pop();
          }
        },
        specialPop(state, page) {
          state.stack.pop();
          state.stack.pop();
          setTimeout(function() { //待たせないと読み込めていない
            state.stack.push(page);
          }, 10);
        },
        returnTop(state) {
          let top = 3;
          let stack = state.stack;
          for (let i = 0; i <= stack.length - top; i++) {
            state.stack.pop();
          }
        },
        stackRefresh(state) {
          let stacks = state.stack;
          // console.log(stacks.length);
          while (state.stack.length > 1) {
            state.stack.shift();
            // console.log(state.stack);
          }
        },        
        toggle(state, shouldOpen) {
          if (typeof shouldOpen === 'boolean') {
            state.open = shouldOpen;
          } else {
            state.open = !state.open;
          }
        },
      },
    },
  },
});

export default store