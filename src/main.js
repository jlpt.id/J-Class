import 'onsenui/css/onsenui.css';
import 'onsenui/css/onsen-css-components.css';
import '@/assets/styles/main.scss';
import 'bootstrap/dist/css/bootstrap.min.css';


import 'bootstrap';
import 'jquery/dist/jquery';
import 'popper.js/dist/popper';

import Vue from 'vue';
import VueOnsen from 'vue-onsenui';
import store from './store';
import App from './App';
import { auth } from './firebase';

Vue.config.productionTip = false;

Vue.use(VueOnsen);

/* eslint-disable no-new */
let app;
auth.onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      el: '#app',
      store,
      render: h => h(App),
    });
  }
});
